//
// Created by Vlad on 10.11.2022.
//
#include "rotate.h"
#include "image.h"

#include <stdlib.h>

struct image rotate(struct image image) {
    struct pixel *rotated_pixel = (struct pixel *) malloc(image.width * image.height * sizeof(struct pixel));
    for (uint64_t i = 0; i < (image.height); i++) {
        for (uint64_t j = 0; j < (image.width); j++) {
            rotated_pixel[(j * image.height) + i] = image.data[((image.height - 1 - i) * image.width) + j];
        }
    }
    return (struct image) {.height = image.width, .width = image.height, .data = rotated_pixel};
}
