//
// Created by Vlad on 10.11.2022.
//

#ifndef THIRDLAB_ROTATE_H
#define THIRDLAB_ROTATE_H

struct image rotate(struct image image);

#endif //THIRDLAB_ROTATE_H
