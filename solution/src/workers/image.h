//
// Created by Vlad on 10.11.2022.
//

#ifndef THIRDLAB_IMAGE_H
#define THIRDLAB_IMAGE_H

#include <stdint.h>

#pragma pack(push, 1)
struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct pixel {
    uint8_t b, g, r;
};
#pragma pack(pop)

struct image create_image(const uint64_t height, const uint64_t width);

void image_free(struct image *img);

#endif //THIRDLAB_IMAGE_H
