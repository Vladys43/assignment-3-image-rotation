//
// Created by Vlad on 10.11.2022.
//

#include "../readers/bmp.h"
#include "../readers/bmp_io.h"

enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header header = {0};
    if (read_header(in, &header)) {
        return READ_INVALID_HEADER;
    }
    struct image image = create_image(header.biHeight, header.biWidth);
    if(image.data == NULL){
        return READ_ERROR;
    }
    if(read_content(in, &image)){
        return READ_INVALID_BITS;
    }
    *img = image;
    return READ_OK;
}

enum write_status to_bmp(FILE *file, struct image *img) {
    if(write_header(file, img)){
        return WRITE_ERROR;
    }
    if(write_content(file,img)){
        return WRITE_ERROR;
    }
    return WRITE_OK;
}
