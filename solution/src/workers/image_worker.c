//
// Created by Vlad on 10.11.2022.
//
#include "image.h"
#include <stdlib.h>

struct image create_image(const uint64_t height, const uint64_t width){
    return (struct image)  {.height = height,.width = width,.data = malloc(sizeof(struct pixel) * height * width)};
}

inline void image_free(struct image *img){
    free(img->data);
    img->data = NULL;
}

