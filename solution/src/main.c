//
// Created by Vlad on 10.11.2022.
//
#include "readers/bmp.h"
#include "readers/file_reader.h"
#include "status_workers/status_worker.h"
#include "workers/rotate.h"

#define INPUT_FILE_MODE "rb"
#define OUTPUT_FILE_MODE "wb";

enum argument_status check_arguments_count(int arg_count) {
    if (arg_count < 3) {
        return NOT_ENOUGH_ARGUMENTS;
    }
    if (arg_count > 3) {
        return TOO_MANY_ARGUMENTS;
    }
    return GREAT_COUNT;
}

void close_file(FILE *file) {
    fclose(file);
}


int main(int arg_count, char **args) {

    if (argument_status_printer(check_arguments_count(arg_count))) {
        return 1;
    }
    char *input_file_name = args[1];
    char *input_file_mode = INPUT_FILE_MODE;
    char *output_file_name = args[2];
    char *output_file_mode = OUTPUT_FILE_MODE;
    FILE *in_file;
    if (file_status_printer(read_file(input_file_name, input_file_mode, &in_file))) {
        close_file(in_file);
        return 1;
    }
    struct image image = {0};
    if (read_status_printer(from_bmp(in_file, &image))) {
        close_file(in_file);
        image_free(&image);
        return 1;
    }
    struct image rotated_image = rotate(image);
    FILE *out_file;
    if (file_status_printer(read_file(output_file_name, output_file_mode, &out_file))) {
        close_file(in_file);
        close_file(out_file);
        image_free(&image);
        image_free(&rotated_image);
        return 1;
    }
    if (write_status_printer(to_bmp(out_file, &rotated_image))) {
        close_file(in_file);
        close_file(out_file);
        image_free(&image);
        image_free(&rotated_image);
        return 1;
    }
    printf("Image was rotated");
    image_free(&image);
    image_free(&rotated_image);
    close_file(out_file);
    close_file(in_file);
    return 0;
}


