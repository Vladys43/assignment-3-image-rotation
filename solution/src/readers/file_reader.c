//
// Created by Vlad on 10.11.2022.
//

#include "file_reader.h"

enum file_status read_file(const char* file_name, const char* mode, FILE** file){
    *file = fopen(file_name,mode);
    if(*file == NULL){
        return INPUT_FILE_ERROR;
    }
    return FILE_OK;
}
