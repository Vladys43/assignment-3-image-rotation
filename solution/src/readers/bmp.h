//
// Created by Vlad on 10.11.2022.
//
#ifndef THIRDLAB_BMP_H
#define THIRDLAB_BMP_H

#include "../status_workers/status.h"
#include "../workers/image.h"

#include <stdio.h>

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

enum read_status from_bmp(FILE *in, struct image *img);

enum write_status to_bmp(FILE *file, struct image *img);

enum default_bmp_header_values{
    default_bfType = 19778,
    default_bfReserved = 0,
    default_biPlanes = 1,
    default_biCompression = 0,
    default_biXPelsPerMeter = 2834,
    default_biYPelsPerMeter =  2834,
    default_biClrUsed = 0,
    default_biClrImportant = 0,
    default_biBitCount = 24,
    default_biSize

};

#endif //THIRDLAB_BMP_H
