//
// Created by Vlad on 10.11.2022.
//

#ifndef THIRDLAB_BMP_IO_H
#define THIRDLAB_BMP_IO_H

#include "bmp.h"

uint64_t read_header(FILE *file, struct bmp_header *header);

enum read_status read_content(FILE *file, struct image *image);

uint64_t write_header(FILE *file, struct image *image);

enum write_status write_content(FILE *file, struct image *image);

#endif //THIRDLAB_BMP_IO_H
