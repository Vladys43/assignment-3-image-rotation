//
// Created by Vlad on 10.11.2022.
//

#include "bmp_io.h"

#define COUNT_OF_BYTES 4

static uint64_t read_padding(const uint64_t width) {
    uint64_t padding = (width * sizeof(struct pixel)) % 4;
    return ((COUNT_OF_BYTES - padding)) % COUNT_OF_BYTES;
}

uint64_t read_header(FILE *file, struct bmp_header *header) {
    return !fread(header, sizeof(struct bmp_header), 1, file);
}

enum read_status read_content(FILE *file, struct image *image) {
    const uint64_t padding = read_padding(image->width);
    struct pixel *data = image->data;
    for (int i = 0; i < image->height; i++) {
        uint64_t count_of_read_bytes = fread(data + image->width * i, sizeof(struct pixel), image->width, file);
        if (count_of_read_bytes != image->width) {
            return READ_ERROR;
        }
        fseek(file, (int8_t) padding, SEEK_CUR);
    }
    return READ_OK;
}

static struct bmp_header create_header(struct image *img) {
    size_t size = img->height * img->width * sizeof(struct pixel);
    return (struct bmp_header) {.bfType=default_bfType, .bfReserved=default_bfReserved, .bOffBits=sizeof(struct bmp_header),
            .biPlanes=default_biPlanes, .biCompression=default_biCompression, .biXPelsPerMeter=default_biXPelsPerMeter,
            .biYPelsPerMeter=default_biYPelsPerMeter, .biClrUsed=default_biClrUsed, .biClrImportant=default_biClrImportant,
            .biBitCount=default_biBitCount, .biHeight=img->height, .biWidth=img->width,
            .biSizeImage =size, .bfileSize= sizeof(struct bmp_header) + size, .biSize=default_biSize};
}

uint64_t write_header(FILE *file, struct image *image) {
    struct bmp_header header = create_header(image);
    return !fwrite(&header, sizeof(struct bmp_header), 1, file);
}

enum write_status write_content(FILE *file, struct image *image) {
    const uint64_t padding = read_padding(image->width);
    const uint64_t empty_space[3] = {0};
    size_t ptr = 0;
    for (size_t i = 0; i < image->height; i++) {
        for (size_t j = 0; j < image->width; j++) {
            if (!fwrite(image->data + ptr, sizeof(struct pixel), 1, file)) {
                return WRITE_ERROR;
            }
            ptr++;
        }
        fwrite(empty_space, padding, 1, file);
    }
    return WRITE_OK;
}
