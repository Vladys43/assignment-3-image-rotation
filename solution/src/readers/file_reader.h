//
// Created by Vlad on 10.11.2022.
//

#ifndef THIRDLAB_FILE_READER_H
#define THIRDLAB_FILE_READER_H

#include "../status_workers/status.h"
#include <stdio.h>

enum file_status read_file(const char* file_name, const char* mode, FILE** file);

#endif //THIRDLAB_FILE_READER_H
