//
// Created by Vlad on 07.01.2023.
//

#ifndef IMAGE_TRANSFORMER_STATUS_WORKER_H
#define IMAGE_TRANSFORMER_STATUS_WORKER_H

#include "status.h"

int file_status_printer(enum file_status status);

int write_status_printer(enum write_status status);

int read_status_printer(enum read_status status);

int argument_status_printer(enum argument_status status);

#endif //IMAGE_TRANSFORMER_STATUS_WORKER_H
