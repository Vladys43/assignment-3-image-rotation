//
// Created by Vlad on 07.01.2023.
//

#include "status_worker.h"


int file_status_printer(enum file_status status) {
    switch (status) {
        case FILE_OK:
            fprintf(stdout, "FILE_OK\n");
            return 0;
        case INPUT_FILE_ERROR:
            fprintf(stderr, "INPUT_FILE_ERROR\n");
            break;
        case OUTPUT_FILE_ERROR:
            fprintf(stderr, "OUTPUT_FILE_ERROR\n");
            break;
    }
    return 1;
}

int write_status_printer(enum write_status status) {
    switch (status) {
        case WRITE_OK:
            fprintf(stdout, "WRITE_OK\n");
            return 0;
            break;
        case WRITE_ERROR:
            fprintf(stderr, "WRITE_ERROR\n");
            break;
    }
    return 1;
}

int read_status_printer(enum read_status status) {
    switch (status) {
        case READ_OK:
            fprintf(stdout, "READ_OK\n");
            return 0;
            break;
        case READ_INVALID_SIGNATURE:
            fprintf(stderr, "READ_INVALID_SIGNATURE\n");
            break;
        case READ_INVALID_BITS:
            fprintf(stderr, "READ_INVALID_BITS\n");
            break;
        case READ_INVALID_HEADER:
            fprintf(stderr, "READ_INVALID_HEADER\n");
            break;

        case READ_ERROR:
            fprintf(stderr, "READ_ERROR\n");
            break;
    }
    return 1;
}

int argument_status_printer(enum argument_status status) {
    switch (status) {
        case GREAT_COUNT:
            fprintf(stdout, "GREAT_COUNT_OF_ARGUMENTS\n");
            break;
        case NOT_ENOUGH_ARGUMENTS:
            fprintf(stderr, "NOT_ENOUGH_ARGUMENTS\n");
            return 1;
            break;
        case TOO_MANY_ARGUMENTS:
            fprintf(stdout, "Arguments after third will be ignored, be ready for this\n");
            break;
    }
    return 0;
}
