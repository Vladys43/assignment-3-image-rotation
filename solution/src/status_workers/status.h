//
// Created by Vlad on 10.11.2022.
//

#ifndef THIRDLAB_STATUS_H
#define THIRDLAB_STATUS_H

#include <stdio.h>

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_ERROR
};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum argument_status {
    GREAT_COUNT = 0,
    TOO_MANY_ARGUMENTS,
    NOT_ENOUGH_ARGUMENTS,
};

enum file_status {
    FILE_OK = 0,
    INPUT_FILE_ERROR,
    OUTPUT_FILE_ERROR
};
#endif //THIRDLAB_STATUS_H
